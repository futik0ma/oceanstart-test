<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $name Наименование товара
 * @property Category[]|Collection $categories Категории товара
 * @property int $price Цена товара (коп)
 * @property bool $is_published Флаг публикации
 */
class Product extends Model
{
    use HasFactory, SoftDeletes;

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }
}
