<?php

namespace App\Http\Requests;

use App\Http\Resources\ValidationErrorsResource;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

abstract class Request extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            (new ValidationErrorsResource($validator->errors()))
                ->response()
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
