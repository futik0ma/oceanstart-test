<?php

namespace App\Http\Resources;

use App\Http\Resources\Resource;

/**
 * Ресурс для вывода ошибок валидации
 */
final class ValidationErrorsResource extends Resource
{
    public function toArray($request)
    {
        return $this->resource;
    }
}
