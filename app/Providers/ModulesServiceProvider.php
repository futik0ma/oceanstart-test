<?php

namespace App\Providers;

use App\Modules\Category\Providers\CategoryProvider;
use App\Modules\Product\Providers\ProductProvider;
use App\Modules\ProductList\Providers\ProductListProvider;
use Illuminate\Support\ServiceProvider;

/**
 * Провайдер для регистрации модулей
 */
final class ModulesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->register(ProductProvider::class);
        $this->app->register(CategoryProvider::class);
        $this->app->register(ProductListProvider::class);
    }
}
