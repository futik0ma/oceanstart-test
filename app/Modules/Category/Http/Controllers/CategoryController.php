<?php

namespace app\Modules\Category\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Modules\Category\Http\Requests\CategoryRequest;
use App\Modules\Category\Http\Resources\CategoryResource;
use App\Modules\Category\Services\CategoryService;
use Illuminate\Http\Response;

final class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function create(CategoryRequest $request)
    {
        $category = $this->categoryService->create($request);

        return (new CategoryResource($category))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function delete(Category $category)
    {
        $this->categoryService->delete($category);

        return null;
    }
}
