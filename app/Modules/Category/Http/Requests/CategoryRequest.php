<?php

namespace App\Modules\Category\Http\Requests;

use App\Http\Requests\Request;

/**
 * @property string $name
 */
final class CategoryRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required|string',
        ];
    }
}
