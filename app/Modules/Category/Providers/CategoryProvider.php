<?php

namespace App\Modules\Category\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

final class CategoryProvider extends RouteServiceProvider
{
    public function boot()
    {
        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace('App\Modules\Category\Http\Controllers')
                ->group(__DIR__ . '/../Routes/api.php');
        });
    }
}
