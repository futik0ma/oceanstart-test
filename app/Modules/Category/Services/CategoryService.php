<?php

namespace App\Modules\Category\Services;

use App\Models\Category;
use App\Modules\Category\Http\Requests\CategoryRequest;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class CategoryService
{
    public function create(CategoryRequest $request): Category
    {
        $category = new Category();
        $category->name = $request->name;
        $category->save();

        return $category;
    }

    public function delete(Category $category): void
    {
        if ($category->products()->count() > 0) {
            throw new HttpException(Response::HTTP_TOO_EARLY, 'Category has products');
        }

        $category->delete();
    }
}
