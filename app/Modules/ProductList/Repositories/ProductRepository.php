<?php

namespace App\Modules\ProductList\Repositories;

use App\Models\Product;
use App\Modules\ProductList\Dto\ProductSearchDto;
use Illuminate\Database\Eloquent\Collection;

final class ProductRepository
{
    /**
     * Поиск
     */
    public function search(ProductSearchDto $dto): Collection
    {
        $query = (new Product());

        // по названию продукта
        if (!is_null($dto->name)) {
            $query = $query
                ->where('name', 'like', '%' . $dto->name . '%');
        }

        // по ИД категории
        if (!is_null($dto->category_id)) {
            $query = $query
                ->whereHas('categories', function ($query) use ($dto) {
                    $query->where('id', $dto->category_id);
                });
        }

        // по названию категории
        if (!is_null($dto->category_name)) {
            $query = $query
                ->whereHas('categories', function ($query) use ($dto) {
                    $query->where('name', 'like', '%' . $dto->category_name . '%');
                });
        }

        // по цене от 
        if (!is_null($dto->price_from)) {
            $query = $query
                ->where('price', '>=', $dto->price_from);
        }

        // по цене до
        if (!is_null($dto->price_to)) {
            $query = $query
                ->where('price', '<=', $dto->price_to);
        }

        // по опубликованности
        if (!is_null($dto->is_published)) {
            $query = $query
                ->where('is_published',  $dto->is_published);
        }

        // по удаленности
        if (!is_null($dto->is_deleted)) {
            if ($dto->is_deleted) {
                $query = $query
                    ->onlyTrashed();
            }
        }

        $products = $query->get();

        return $products;
    }
}
