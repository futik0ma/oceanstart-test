<?php

namespace App\Modules\ProductList\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

final class ProductListProvider extends RouteServiceProvider
{
    public function boot()
    {
        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace('App\Modules\ProductList\Http\Controllers')
                ->group(__DIR__ . '/../Routes/api.php');
        });
    }
}
