<?php

namespace App\Modules\ProductList\Transformers;

use App\Modules\ProductList\Dto\ProductSearchDto;
use App\Modules\ProductList\Http\Requests\ProductsSearchRequest;

final class ProductTransformer
{
    public function productSearchRequest2productSearchDto(
        ProductsSearchRequest $request
    ): ProductSearchDto {
        $dto = new ProductSearchDto();

        $dto->name = $request->name;
        $dto->category_id = $request->category_id;
        $dto->category_name = $request->category_name;
        $dto->price_from = $request->price_from;
        $dto->price_to = $request->price_to;
        $dto->is_published = $request->is_published;
        $dto->is_deleted = $request->is_deleted;

        return $dto;
    }
}
