<?php

namespace App\Modules\ProductList\Http\Resources;

use App\Http\Resources\Resource;

final class ProductResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'price'        => $this->price,
            'is_published' => $this->is_published,
            'is_deleted'   => $this->trashed(),
        ];
    }
}
