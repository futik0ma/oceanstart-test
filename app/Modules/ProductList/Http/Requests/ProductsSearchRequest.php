<?php

namespace App\Modules\ProductList\Http\Requests;

use App\Http\Requests\Request;

/**
 * @property string|null $name
 * @property int|null    $category_id
 * @property string|null $category_name
 * @property int|null    $price_from
 * @property int|null    $price_to
 * @property bool|null   $is_published
 * @property bool|null   $is_deleted
 */
final class ProductsSearchRequest extends Request
{
    public function rules()
    {
        return [
            'name'          => 'string',
            'category_id'   => 'integer|exists:categories,id',
            'category_name' => 'string',
            'price_from'    => 'integer',
            'price_to'      => 'integer',
            'is_published'  => 'boolean',
            'is_deleted'    => 'boolean'
        ];
    }
}
