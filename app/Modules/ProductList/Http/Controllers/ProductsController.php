<?php

namespace App\Modules\ProductList\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ProductList\Http\Requests\ProductsSearchRequest;
use App\Modules\ProductList\Http\Resources\ProductResource;
use App\Modules\ProductList\Repositories\ProductRepository;
use App\Modules\ProductList\Transformers\ProductTransformer;

final class ProductsController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ProductTransformer
     */
    private $productTransformer;

    public function __construct(
        ProductRepository $productRepository,
        ProductTransformer $productTransformer
    ) {
        $this->productTransformer = $productTransformer;
        $this->productRepository = $productRepository;
    }

    /**
     * Поиск продуктов
     */
    public function search(ProductsSearchRequest $request)
    {
        $products = $this->productRepository->search(
            $this->productTransformer->productSearchRequest2productSearchDto(
                $request
            )
        );

        return ProductResource::collection($products);
    }
}
