<?php

namespace App\Modules\ProductList\Dto;

final class ProductSearchDto
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var int|null
     */
    public $category_id;

    /**
     * @var string|null
     */
    public $category_name;

    /**
     * @var int|null
     */
    public $price_from;

    /**
     * @var int|null
     */
    public $price_to;

    /**
     * @var bool|null
     */
    public $is_published;

    /**
     * @var bool|null
     */
    public $is_deleted;
}
