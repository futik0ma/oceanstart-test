<?php

namespace App\Modules\Product\Services;

use App\Models\Product;
use App\Modules\Product\Http\Requests\ProductRequest;

final class ProductService
{
    /**
     * Создание
     */
    public function create(ProductRequest $request): Product
    {
        // продукт
        $product = new Product();

        $product->name = $request->name;
        $product->price = $request->price;
        $product->is_published = $request->is_published;

        $product->save();

        // категории
        $product->categories()->attach($request->categories);

        return $product;
    }

    /**
     * Обновление
     */
    public function update(Product $product, ProductRequest $request): Product
    {
        // продукт
        $product->name = $request->name;
        $product->price = $request->price;
        $product->is_published = $request->is_published;

        $product->update();

        // категории
        $product->categories()->sync($request->categories);

        return $product;
    }

    /**
     * Удаление
     */
    public function delete(Product $product): void
    {
        $product->delete();
    }
}
