<?php

namespace App\Modules\Product\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

final class ProductProvider extends RouteServiceProvider
{
    public function boot()
    {
        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace('App\Modules\Product\Http\Controllers')
                ->group(__DIR__ . '/../Routes/api.php');
        });
    }
}
