<?php

namespace App\Modules\Product\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Modules\Product\Http\Requests\ProductRequest;
use App\Modules\Product\Http\Resources\ProductResource;
use App\Modules\Product\Services\ProductService;
use Illuminate\Http\Response;

final class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function create(ProductRequest $request)
    {
        $product = $this->productService->create($request);

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function update(Product $product, ProductRequest $request)
    {
        $product = $this->productService->update($product, $request);

        return new ProductResource($product);
    }

    public function delete(Product $product)
    {
        $this->productService->delete($product);

        return null;
    }
}
