<?php

namespace App\Modules\Product\Http\Requests;

use App\Http\Requests\Request;

/**
 * @property string $name
 * @property int $price
 * @property bool $is_published
 * @property int[] $categories
 */
final class ProductRequest extends Request
{
    public function rules()
    {
        return [
            'name'         => 'required|string',
            'price'        => 'required|integer|min:0',
            'is_published' => 'required|boolean',
            'categories'   => 'required|array|min:2|max:10',
            'categories.*' => 'required|integer|exists:categories,id'
        ];
    }
}
