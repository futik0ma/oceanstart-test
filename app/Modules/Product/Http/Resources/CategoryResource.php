<?php

namespace App\Modules\Product\Http\Resources;

use App\Http\Resources\Resource;

final class CategoryResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
        ];
    }
}
