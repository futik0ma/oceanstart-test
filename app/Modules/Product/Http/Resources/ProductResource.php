<?php

namespace App\Modules\Product\Http\Resources;

use App\Http\Resources\Resource;

final class ProductResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'price'        => $this->price,
            'is_published' => $this->is_published,
            'categories'   => CategoryResource::collection($this->categories)
        ];
    }
}
